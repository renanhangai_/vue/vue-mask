import { MaskManager } from "./MaskManager";


export default {
	install( vue, options ) {
		options = options || {};
		const manager = getMaskManager( vue, options );
		vue.directive( 'mask', ( el, binding ) => {
			manager.apply( el, binding );
		})
	},
}

function getMaskManager( vue, options ) {
	let manager = options.manager;
	if ( !manager ) {
		if ( !vue.MaskManager ) {
			vue.MaskManager = new MaskManager( vue, options );
			if ( options.masks ) {
				for ( const key in options.masks )
					vue.MaskManager.register( key, options.masks[key] );
			}
		}
		manager = vue.MaskManager;
	}
	return manager;
}