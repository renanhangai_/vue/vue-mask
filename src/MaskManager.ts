import {mask as VMask} from 'vue-the-mask'
import {VMoney} from 'v-money';

export class MaskManager {

	readonly vue: any;
	readonly options: any;
	readonly masks: { [key: string]: string|[string] };
	
	constructor( vue, options ) {
		this.vue = vue;
		this.options = options;
		this.masks = {};
	}

	register( mask: string, definition: string|[string] ) {
		this.masks[mask] = definition;
	}
	
	processMask( binding ): any {
		let mask = null;
		let options = null;
		if ( binding.arg ) {
			mask    = binding.arg;
			options = binding.value;
		} else if ( binding.value ) {
			if ( typeof(binding.value) === 'string' ) { 
				mask = binding.value;
			} else if ( typeof(binding.value) === 'object' ) { 
				mask = binding.value.mask;
				options = binding.value;
			}
		}
		if ( mask )
			mask = this.masks[mask] || mask;
		return { mask, options };
	}
	
	apply( el, binding ): void {
		if ( binding.value === false )
			return;

		const { mask, options: maskOptions } = this.processMask( binding );
		if ( !mask )
			return;
		else if ( mask === 'decimal' ) {
			const options = {
				decimal: ",",
				thousands: ".",
				precision: 2,
			};
			if ( typeof(maskOptions) === 'number' ) {
				options.precision = maskOptions;
			} else if ( typeof(maskOptions) === 'object' ) {
				Object.assign( options, maskOptions );
			}
			binding.value = options;
			this.vue.nextTick( () => {
				VMoney( el, binding );
			});
			return;
		}

		binding.value = mask;
		this.vue.nextTick( () => {
			VMask( el, binding );
		});
	}

}